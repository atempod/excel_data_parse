import os
import unittest
from schedule_excel_parse \
    import clean_string, clean_list, sched2dict, pickle_dump, pickle_load, \
            excel_write_from_list, excel2raw_list


class TestExcelWriteRead(unittest.TestCase):

    def setUp(self):
        self.excel_file = 'test_excel_write_read.xlsx'

    def test_file_not_found(self):
        data_to_save_excel = []
        excel_write_from_list(data_to_save_excel, self.excel_file)
        data_loaded_from_excel = excel2raw_list("erroneous_file_name_for_test")
        self.assertEqual(data_loaded_from_excel, None)

    def test_empty_list(self):
        data_to_save_excel = []
        excel_write_from_list(data_to_save_excel, self.excel_file)
        data_loaded_from_excel = excel2raw_list(self.excel_file)
        self.assertEqual(data_loaded_from_excel, [[None]])

    def test_several_enclosed_empty_lists(self):
        data_to_save_excel = [[], [], []]
        excel_write_from_list(data_to_save_excel, self.excel_file)
        data_loaded_from_excel = excel2raw_list(self.excel_file)
        self.assertEqual(data_loaded_from_excel, [[None]])

    def test_one_level_list(self):
        data_to_save_excel = ['a', 'b']
        excel_write_from_list(data_to_save_excel, self.excel_file)
        data_loaded_from_excel = excel2raw_list(self.excel_file)
        self.assertEqual(data_loaded_from_excel, [data_to_save_excel])

    def test_one_level_tuple(self):
        data_to_save_excel = ('a', 'b')
        excel_write_from_list(data_to_save_excel, self.excel_file)
        data_loaded_from_excel = excel2raw_list(self.excel_file)
        self.assertEqual(data_loaded_from_excel, [list(data_to_save_excel)])

    def test_several_enclosed_lists(self):
        data_to_save_excel = [['a', 'b'], ['a', 'b']]
        excel_write_from_list(data_to_save_excel, self.excel_file)
        data_loaded_from_excel = excel2raw_list(self.excel_file)
        self.assertEqual(data_loaded_from_excel, data_to_save_excel)

    def tearDown(self):
        try:
            os.remove(self.excel_file)
        except FileNotFoundError as e:
            print('File not found', repr(e))


class CleanStringTest(unittest.TestCase):

    def test_str_val(self):
        str_val = '  Nombre  Termi-\nnal\nDes- \nti-     \nno  '
        self.assertEqual(clean_string(str_val), 'Nombre Terminal Destino')

    def test_any_type_available(self):
        bool_val = True
        none_val = None
        int_val = 10
        self.assertEqual(clean_string(bool_val), bool_val)
        self.assertEqual(clean_string(none_val), none_val)
        self.assertEqual(clean_string(int_val), int_val)


class CleanListTest(unittest.TestCase):

    def test_delete_enclosed_list_with_none_first_element(self):
        list_val = [[1,2], [None, 4], ['a', 'b']]
        self.assertEqual(clean_list(list_val), [[1,2], ['a', 'b']])

    def test_delete_enclosed_list_with_empty_string_first_element(self):
        list_val = [[1,2], ['', 4], ['a', 'b']]
        self.assertEqual(clean_list(list_val), [[1,2], ['a', 'b']])

    def test_delete_enclosed_list_with_space_only_first_element(self):
        list_val = [[1,2], [' ', 4], ['a', 'b']]
        self.assertEqual(clean_list(list_val), [[1,2], ['a', 'b']])


class Sched2DictTest(unittest.TestCase):

    def test_empty_list(self):
        input_data = []
        self.assertDictEqual(sched2dict(input_data), {})

    def test_list_to_dict(self):
        input_val = [['', ''], ['', '']]
        self.assertDictEqual(sched2dict(input_val), {})

    def test_start_table_one(self):
        input_data = [
            ['Empresa:', 'Alianza', None, None, None, None, None, None, None],
            ['Servicio:', 'semicama', None, None, None, None, None, None, None]]
        self.assertDictEqual(sched2dict(input_data), {'company01':
            {'tab_common': {'Empresa:': 'Alianza', 'Servicio:': 'semicama'}}})

    def test_start_table_two(self):
        input_data = [
            ['Empresa:', 'Alianza', None, None, None, None, None, None, None],
            ['Provincia Origen', None, None, None, None, None, None, None, None],
            ['Azuay', 'Cuenca', 3, 4, 5, 6, 7, 8, 9]]
        result_data = {'company01':
             {'tab_common': {'Empresa:': 'Alianza'},
             'tab_sched': {'Provincia Origen': 'Azuay',
                           'Cantón Origen': 'Cuenca',
                           'Nombre Terminal Origen': 3,
                           'Provincia Destino': 4,
                           'Cantón Destino': 5,
                           'Nombre Terminal Destino': 6,
                           'Hora Salida': 7,
                           'Duración': 8,
                           'Precio': 9}}}
        self.assertDictEqual(sched2dict(input_data), result_data)

    def test_12_enclosed_lists(self):
        input_data = [['Empresa', ''], ['Empresa', ''], ['Empresa', ''],
                      ['Empresa', ''], ['Empresa', ''], ['Empresa', ''],
                      ['Empresa', ''], ['Empresa', ''], ['Empresa', ''],
                      ['Empresa', ''], ['Empresa', ''], ['Empresa', '']]

        result_data = {'company01': {'tab_common': {'Empresa': ''}},
                        'company02': {'tab_common': {'Empresa': ''}},
                        'company03': {'tab_common': {'Empresa': ''}},
                        'company04': {'tab_common': {'Empresa': ''}},
                        'company05': {'tab_common': {'Empresa': ''}},
                        'company06': {'tab_common': {'Empresa': ''}},
                        'company07': {'tab_common': {'Empresa': ''}},
                        'company08': {'tab_common': {'Empresa': ''}},
                        'company09': {'tab_common': {'Empresa': ''}},
                        'company10': {'tab_common': {'Empresa': ''}},
                        'company11': {'tab_common': {'Empresa': ''}},
                        'company12': {'tab_common': {'Empresa': ''}}}
        self.assertDictEqual(sched2dict(input_data), result_data)


class PickleDumpLoadTest(unittest.TestCase):

    def setUp(self):
        self.pickle_file = 'test_pickle_data'

    def test_file_not_found(self):
        data_to_dump_pickle = []
        pickle_dump(data_to_dump_pickle, self.pickle_file)
        data_after_load_pickle = pickle_load("erroneous_file_name_for_test")
        self.assertEqual(data_after_load_pickle, None)

    def test_empty_list(self):
        data_to_dump_pickle = []
        pickle_dump(data_to_dump_pickle, self.pickle_file)
        data_after_load_pickle = pickle_load(self.pickle_file)
        self.assertEqual(data_to_dump_pickle, data_after_load_pickle)

    def test_empty_dict(self):
        data_to_dump_pickle = {}
        pickle_dump(data_to_dump_pickle, self.pickle_file)
        data_after_load_pickle = pickle_load(self.pickle_file)
        self.assertEqual(data_after_load_pickle, data_to_dump_pickle)

    def test_one_level_list(self):
        data_to_dump_pickle = ['a', True, None]
        pickle_dump(data_to_dump_pickle, self.pickle_file)
        data_after_load_pickle = pickle_load(self.pickle_file)
        self.assertEqual(data_after_load_pickle, data_to_dump_pickle)

    def test_three_level_list(self):
        data_to_dump_pickle = [[['a','b','c'],['a', True, None],['a','b','c']],
                            [['a', 'b', 'c'], ['a', 'b', 'c'], ['a', 'b', 'c']],
                            [['a', 'b', 'c'], ['a', 'b', 'c'], ['a', 'b', 'c']]]
        pickle_dump(data_to_dump_pickle, self.pickle_file)
        data_after_load_pickle = pickle_load(self.pickle_file)
        self.assertEqual(data_after_load_pickle, data_to_dump_pickle)

    def tearDown(self):
        try:
            os.remove(self.pickle_file)
        except FileNotFoundError as e:
            print('File not found', repr(e))


if __name__ == '__main__':
    unittest.main()
