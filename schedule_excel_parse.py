from openpyxl import load_workbook
from openpyxl.workbook import Workbook
import pickle
import re

excel_file = './data_files/schedule_data.xlsx'
data_file = './data_files/data_pickle'

TITLE_TABLE2 = ['Provincia Origen',
               'Cantón Origen',
               'Nombre Terminal Origen',
               'Provincia Destino',
               'Cantón Destino',
               'Nombre Terminal Destino',
               'Hora Salida',
               'Duración',
               'Precio',
               ]

START_TABLE1 = 'Empresa'
START_TABLE2 = 'Provincia'


def excel_write_from_list(list_data, excel_file):
    """ Write data from list to the actual worksheet of the excel file. """
    wb = Workbook()
    ws1 = wb.active
    ws1.title = 'from_list'

    list_dim2 = any(isinstance(a, (list, tuple)) for a in list_data)

    if not list_dim2:
        ws1.append(list_data)
    # if array_dim == 2:
    else:
        for row in list_data:
            ws1.append(row)
    wb.save(excel_file)


def excel2raw_list(excel_file):
    """
    Reads actual worksheet from excel file.
    Returns it as a raw two dimensional list.
    """
    sched_raw_list = []
    try:
        wb = load_workbook(excel_file, read_only=True)
        ws = wb.active
    except FileNotFoundError as e:
        print('File was not found: => excel2raw_list => Return None => ', repr(e))
        return

    for row in range(1, ws.max_row + 1):
        # if row > 120: break
        row_list = []
        for col in range (1, ws.max_column + 1):
            row_list.append(ws.cell(row, col).value)
        sched_raw_list.append(row_list)
    return sched_raw_list


def pickle_dump(data, pickle_file):
    """ Dump data to the pickle data file """
    with open(pickle_file, 'wb') as f:
        pickle.dump(data, f)


def pickle_load(data_file):
    """ Load data temporarily restored to the pickle file """
    try:
        with open(data_file, 'rb') as f:
            return pickle.load(f)
    except FileNotFoundError as e:
        print('File was not found: => pickle_load => Return None => ', repr(e))
        return


def sched2dict(sched_list):
    """ Makes enclosed dicts from enclosed lists """
    tab, company, sched_num, sched_dict = ('', '', 0, {})
    # if len(sched_list) == 0:
    #     return {}
    for row_num, row in enumerate(sched_list):
        if row[0].startswith(START_TABLE1):
            sched_num += 1
            if sched_num < 10:
                company = "company" + '0' + str(sched_num)
            else:
                company = "company" + str(sched_num)
            tab = 'tab_common'
            sched_dict[company] = {}
            sched_dict[company][tab] = {}
        elif row[0].startswith(START_TABLE2):
            tab = 'tab_sched'
            sched_dict[company][tab] = {}
            continue

        if tab == 'tab_common':
            sched_dict[company][tab][row[0]] = row[1]

        if tab == 'tab_sched':
            sched_dict[company][tab] = dict(zip(TITLE_TABLE2, row))

    return sched_dict


def clean_string(line):
    """ Cleans string from hyphens, new lines and extra spaces
    # >>> clean_string('  Nombre  Termi-\\nnal\\nDestino  ')
    # 'Nombre Terminal Destino'
    """
    try:
        line = re.sub(r'-\s*\n', '', line)
        line = ' '.join(line.split())
    except TypeError as e: # Catched not string objects
        return line
    return line


def clean_list(init_list):
    """ Cleans list from enclosed lists with empty first element ('empty list')
        Starts the clean_string function
    """
    for i in reversed(range(len(init_list))):
        for col_num, cell in enumerate(init_list[i]):
            init_list[i][col_num] = clean_string(cell)
        if init_list[i][0] is None:
            del init_list[i]
            continue
        elif  init_list[i][0] == '':
            del init_list[i]
            continue
    return init_list


if __name__ == "__main__":
    import time
    start_time = time.time()
    import doctest
    doctest.testmod()

    ### Read data from Excel file
    # sched_list = excel2raw_list(excel_file)
    # print("--- %s seconds ---" % (time.time() - start_time))

    ### Dump data into pickle data file
    # pickle_dump(sched_list, data_file)

    sched_list = pickle_load(data_file)
    print("--- %s seconds ---" % (time.time() - start_time))

    sched_list = clean_list(sched_list)
    # print(sched_list)
    sched_dict = sched2dict(sched_list)
    print(sched_dict)
    print("--- %s seconds ---" % (time.time() - start_time))

    # ([print(row[0]) for row in sched_list])
    # ([print(row) for row in sched_list])
    # print("--- %s seconds ---" % (time.time() - start_time))

